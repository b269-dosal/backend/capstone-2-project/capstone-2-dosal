const express = require("express");
const auth = require("../auth");
const router = express.Router();
const userManipulator = require("../controllers/userManipulator");

// User Registration
router.post("/userRegistration", (request, respond) => {
	userManipulator.user_registration(request.body).then(resFromManipulator => respond.send(resFromManipulator));
});

// View ALl user
router.get("/allUser", (request, respond) => {
	userManipulator.all_user().then(resFromManipulator => respond.send(resFromManipulator));
});

// View user by via email
router.post("/emailcheck", (request, respond) => {
	userManipulator.email_checker(request.body).then(resFromManipulator => respond.send(resFromManipulator));
});

// Login User
router.post("/login", (request, respond) => {
	userManipulator.loginUser(request.body).then(resFromManipulator => respond.send(resFromManipulator));
});

router.post("/:userId/userDetails",/* auth.verify,*/ (request, respond) => {
	const userId = request.params.userId;

	const data = {
		userId: userId
	  };

		/*const decodedToken = auth.decode(request.headers.authorization);
    		if (decodedToken.userId !== userId && decodedToken.isAdmin) {
        	return respond.send({ message: 'Unauthorized' });
    }*/

	userManipulator.getUserDetails(data).then(resFromManipulator => respond.send(resFromManipulator));
});
 

/// UserChangeToAdmin
router.post("/setAdmin", (request, respond) => {
	
	userManipulator.setAdmin(request.body).then(resFromManipulator => respond.send(resFromManipulator));
});

////
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userManipulator.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});


//////////////// Add Ons for Array user All purchased

router.post("/purchaseUser", auth.verify, (request, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: request.body.productId
	}
	userManipulator.purchasedArray(data).then(resultFromController => res.send(resultFromController));
});

/////////////////

router.get("/allNonAdmin", (request, respond) => {
	userManipulator.getAllNonAdminUsers().then(resFromManipulator => respond.send(resFromManipulator));
});



///////////// for updating
router.put('/updateUser/:userId', (request, respond) => {
	const userId = request.params.userId;
	const requestBody = request.body;

	userManipulator.updateUser(userId, requestBody).then(resultFromController => respond.send(resultFromController));
});

///////////////
router.post('/updatePassword', (request, respond) => {

	let data = {
		email: request.body.email,
		password: request.body.password
	}
	
  
	userManipulator.checkAndUpdatePassword(data).then(resultFromController => respond.send(resultFromController));
});
  

module.exports = router;


