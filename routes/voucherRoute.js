const express = require('express');
const router = express.Router();
const voucherManipulator = require('../controllers/voucherManipulator');

router.post('/vouchers', voucherManipulator.createVoucher);
router.get('/vouchers', voucherManipulator.getVouchers);
router.get('/vouchers/:id', voucherManipulator.getVoucher);
router.patch('/vouchers/:id', voucherManipulator.updateVoucher);
router.delete('/vouchers/:id', voucherManipulator.deleteVoucher);

module.exports = router;
