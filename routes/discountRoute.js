const express = require('express');
const router = express.Router();
const discountManipulator = require('../controllers/discountManipulator');

// Create a new discount
router.post('/', discountManipulator.createDiscount);

// Get all discounts
router.get('/', discountManipulator.getDiscounts);

// Get a discount by id
router.get('/:id', discountManipulator.getDiscountById);

// Update a discount by id
router.patch('/:id', discountManipulator.updateDiscount);

// Delete a discount by id
router.delete('/:id', discountManipulator.deleteDiscount);

module.exports = router;
