const express = require("express");
const auth = require("../auth");
const router = express.Router();
const productManipulator = require("../controllers/productManipulator");

// Add Product
router.post("/addproduct", auth.verify, (request, respond) => {
	const data = {	
		product: request.body,	
		isAdmin: auth.decode(request.headers.authorization).isAdmin
				}
		productManipulator.addProduct(data).then(resFromManipulator => respond.send(resFromManipulator));
});

// View ALl Product
router.get("/allProduct", (request, respond) => {
	productManipulator.AllProduct().then(resFromManipulator => respond.send(resFromManipulator));
});

// Find product by ID
router.post("/:productId", (request, respond) => {
	productManipulator.getProduct(request.params).then(resFromManipulator => respond.send(resFromManipulator));
});

// Update Product by ADMIN only
router.put("/:productId", auth.verify, (request, respond) => {
	
	const data = {	
			body: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
	} 
	productManipulator.updateProduct(data, request.params ).then(resFromManipulator => respond.send(resFromManipulator));
});

// Archive Product access only by admin 
router.patch("/:productID/archive", auth.verify, (request, respond) => {

	const data = {	
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	} 
	productManipulator.archiveProduct(data, request.params).then(resFromManipulator => respond.send(resFromManipulator));
});

//
router.patch("/:productID/unarchive", auth.verify, (request, respond) => {

	const data = {	
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	} 
	productManipulator.unarchiveProduct(data, request.params).then(resFromManipulator => respond.send(resFromManipulator));
});



// View all Active product
router.get("/active", (request, respond) => {
	productManipulator.activeProduct().then(resFromManipulator => respond.send(resFromManipulator));
});

// View Product by name
router.get("/keywordsearch", auth.verify, (request, respond) => {
	
	const data = {	
			body : request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
	} 
	productManipulator.ProductSearch_KeyWord(data).then(resFromManipulator => respond.send(resFromManipulator));
}); 


router.delete("/:productID", auth.verify, (request, respond) => {
  const data = {	
    body: request.body,
    isAdmin: auth.decode(request.headers.authorization).isAdmin
  };
  
  productManipulator.deleteProduct(data, request.params.productID)
    .then(resFromManipulator => respond.send(resFromManipulator))
    .catch(error => respond.status(500).send(error.message));
});








module.exports = router;