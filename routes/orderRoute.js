const express = require("express");
const auth = require("../auth");
const router = express.Router();
const orderManipulator = require("../controllers/orderManipulator");

// [NON-ADMIN] Create Order from non Admin
router.post("/createOrder/:userId", auth.verify, (request, respond) => {
    const userId = request.params.userId;
    const data = {
      userId: userId,
      productId: request.body.productId,
      quantity: request.body.quantity
    };
  
    // check if user is not the owner of the order or an admin
    const decodedToken = auth.decode(request.headers.authorization);
    if (decodedToken.userId !== userId && decodedToken.isAdmin) {
        return respond.send({ message: 'Unauthorized' });
    }
  
    orderManipulator.addOrder(data).then(resFromManipulator => respond.send(resFromManipulator));
  });
  
// [ADMIN] View All Orders
router.get("/allOrders", auth.verify, (request, respond) => {
    const data = {        
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
	orderManipulator.viewAllOrders(data).then(resFromManipulator => respond.send(resFromManipulator));
}); 

////////////////////
/* Add All CHECKOUT
router.post("/checkout", (request, respond) => {
    const data = {
        userId: request.body.userId       
    }
    orderManipulator.checkOut_Status_Complete(data).then(resFromManipulator => respond.send(resFromManipulator));
}); */
/////////////////////

// [ADMIN] View Cart of Users Order
router.get("/cartItems/:userId", auth.verify, (request, respond) => {
    const userId = request.params.userId  
    const data = {
        userId: userId,
        isAdmin: auth.decode(request.headers.authorization).isAdmin    
  }
  orderManipulator.viewIncompleteOrders(data).then(resFromManipulator => respond.send(resFromManipulator));
});

//// [NON-ADMIN] View Cart for Non Admin
router.get("/cartItems2/:userId", auth.verify, (request, respond) => {
    const userId = request.params.userId  
    const data = {
        userId: userId,
      //  isAdmin: auth.decode(request.headers.authorization).isAdmin 
  }
const decodedToken = auth.decode(request.headers.authorization);
    if (decodedToken.userId !== userId && decodedToken.isAdmin) {
        return respond.send({ message: 'Unauthorized' });
    }

   orderManipulator.viewIncompleteOrders2(data).then(resFromManipulator => respond.send(resFromManipulator));
});
///

// [ADMIN] Delete Order from cart
router.delete("/deleteOrderCart/:userId", auth.verify, (request, respond) => {
    const userId = request.params.userId
    const data = {
        userId: userId,
        orderId: request.body.orderId,
        //isAdmin: auth.decode(request.headers.authorization).isAdmin      
    }
    const decodedToken = auth.decode(request.headers.authorization);
    if (decodedToken.userId !== userId && decodedToken.isAdmin) {
        return respond.send({ message: 'Unauthorized' });
    }

    orderManipulator.deleteOrder(data).then(resFromManipulator => respond.send(resFromManipulator));
  });

// [NON-ADMIN] Change Quantity of Certain order
router.post("/updateQuantity/:userId", auth.verify, (request, respond) => {
        userId = request.params.userId;
        const data = {
        userId: userId,
        orderId: request.body.orderId,
        quantity:  request.body.quantity 
        //isAdmin: auth.decode(request.headers.authorization).isAdmin    
    }
 // check if user is not the owner of the order or an admin
    const decodedToken = auth.decode(request.headers.authorization);
    if (decodedToken.userId !== userId && decodedToken.isAdmin) {
        return respond.send({ message: 'Unauthorized' });
    }

    orderManipulator.updateOrderQuantity(data).then(resFromManipulator => respond.send(resFromManipulator));
  });

// [NON-ADMIN] Checkout Latest Version with Stocks Update  ///////////
router.post("/checkout2/:userId", auth.verify, (request, respond) => {
    userId = request.params.userId;
    const data = {
        userId: userId,
        productId: request.body.productId
      
        //isAdmin: auth.decode(request.headers.authorization).isAdmin       
    } 
         const decodedToken = auth.decode(request.headers.authorization);
    if (decodedToken.userId !== userId && decodedToken.isAdmin) {
        return respond.send({ message: 'Unauthorized' });
    }

        orderManipulator.checkOutAndUpdateStock(data).then(resFromManipulator => respond.send(resFromManipulator));
});

/////// Transaction to save user specific  - naka array nga pala ung
router.get("/transactionUser/:userId", /* auth.verify,*/ (request, respond) => {
    
  const userId = request.params.userId
    const data = {
        userId: userId 
    }

  /*   const decodedToken = auth.decode(request.headers.authorization);
    if (decodedToken.userId !== userId && decodedToken.isAdmin) {
        return respond.send({ message: 'Unauthorized' });
    }*/
    

    orderManipulator.transactionUser(data).then(resFromManipulator => respond.send(resFromManipulator));
});
/////////////////  Version userbody
router.post("/transactionUser",  auth.verify, (request, respond) => {
  /*   const decodedToken = auth.decode(request.headers.authorization);
    if (decodedToken.userId !== userId && decodedToken.isAdmin) {
        return respond.send({ message: 'Unauthorized' });
    }*/
    
    orderManipulator.transactionUser(request.body).then(resFromManipulator => respond.send(resFromManipulator));
});
/////////////////


/// Showing Transactions
router.post("/transaction", (request, respond) => {
	orderManipulator.transactionViewer().then(resFromManipulator => respond.send(resFromManipulator));
});

router.post("/transaction2", (request, respond) => {
    orderManipulator.transactionViewer2().then(resFromManipulator => respond.send(resFromManipulator));
});


module.exports = router;

