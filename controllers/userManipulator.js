// Dependencies
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration 
module.exports.user_registration = (request_Body) => {
	return User.findOne({email: request_Body.email}).then(existingUser => {
		if (existingUser) {
			//const exist_User = `Email already registered`
			return false
		} else {
			let newUser = new User({
		        first_Name: request_Body.first_Name,
		        last_Name: request_Body.last_Name,
		        email: request_Body.email,
		        mobileNo: request_Body.mobileNo,
		        password: bcrypt.hashSync(request_Body.password, 11)
			});
			return newUser.save().then((user, error) => {
				if (error) {
					return false;
				} else {
					//let newUser = `New Email has been registered`
					console.log(user)
					return true
				};
			});
		}
	});
};

// View All User
module.exports.all_user = () => {
 
	return User.find({}).then(result => {

    /*    const numUsers = result.length;
        const message = `Number of existing users: ${numUsers}`; 
      	return {message, Users: result}*/

      	/*const numUsers = result.length;
        const message = `Number of existing users: ${numUsers}`;*/ 
      	return result
                
	})
}
//////////////

///////////



// Email checker
module.exports.email_checker = (req_body) => {
		return User.find({email : req_body.email}).then(result => {
			if (result.length > 0) {
				//let email = req_body.email;
				//const Message = `Email Exist`;
				//return {Email: email, Message};
				return true
			} else {
				//const email = req_body.email;
				//let Message = `Does not Exist`;
				return false
			};
	});
};

// Login User
module.exports.loginUser = (req_body) => {
	return User.findOne({email: req_body.email}).then(result =>{
		if (result == null){
			
			return false
	
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(req_body.password, result.password);
				if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			//	return result
			} else {
				
				return false
			};
		};
	});
};


// Get Request
module.exports.getUserDetails = (data) => {
		return User.findById(data.userId).then(result => {
			result.password = "";
			return result;
		});
	};
	
// User Change To Admin
module.exports.setAdmin = (req_body) => {
		return User.findOne({email: req_body.email}).then(result =>{
		  if (result == null){
			let email = req_body.email;
			const Message = `Does not Exist`;
			return {Email: email, Message};
	  
		  } else {
			const isPasswordCorrect = bcrypt.compareSync(req_body.password, result.password);
			if (isPasswordCorrect) {
			  if (result.isAdmin === true) {
				// User is already an admin, return access token
				return {access: auth.createAccessToken(result)}
			  } 
			  else if (result.isAdmin === false){
				// Update user's role to 'admin'
				result.isAdmin = true;
				email = result.email ;
				return result.save().then(updatedUser => {
				  return {access: auth.createAccessToken(updatedUser), ChangetoAdmin: email}
				});
			  }
			} else {
			  let message = `Incorrect Password`;
			  return {Message: message}
			};
		  };
		});
	  };

//////////////
module.exports.getProfile = (userData) => {
		return User.findById(userData.id).then(result => {
			if (result == null) {
				return false
			} else {
				result.password = "*****"
				// Returns the user information with the password as an empty string or asterisk.
				return result
			}
		})
	};


////////////  User Array for Products Purchased
module.exports.purchasedArray = async (data) => {
	if (!data)
{

	let userPurchased = await User.findById(data.userId).then(user => {		
		user.purchased.push({productId: data.productId});	
		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				console.log(user)
				return true;
			};
		});
	});

	/*let isCourseUpdated = await Product.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});*/

	if(userPurchased){
		return true;
	} else {
		return false;
	}
}
			let message = Promise.resolve('Non Admin Only')
				return message.then((value) => {
					return {value}
				}) 

};


//// Choosing Non Admin
module.exports.getAllNonAdminUsers = () => {
	return User.find({ isAdmin: false }).then(results => {
	  /*const numUsers = results.length;
	  const message = `Number of non-admin users: ${numUsers}`; 
	  return { message, Users: results };*/
	  return results;
	});
  };
  

///// Update Users Details

module.exports.updateUser = (userId, requestBody) => {
	return User.findByIdAndUpdate(userId, {
		first_Name: requestBody.first_Name,
		last_Name: requestBody.last_Name,
		email: requestBody.email,
		mobileNo: requestBody.mobileNo,
		isAdmin: requestBody.isAdmin,
		password: requestBody.password ? bcrypt.hashSync(requestBody.password, 11) : undefined
	}, { new: true }).then(updatedUser => {
		if (!updatedUser) {
			return Promise.reject('User not found');
		} else {
			console.log(updatedUser);
			return updatedUser;
		}
	}).catch(error => {
		return Promise.reject(error);
	});
};


//// Forgot Password function


module.exports.checkAndUpdatePassword = async (data) => {
	const email = data.email;
	const password = data.password;
	console.log(email);
	console.log(password);
  
	if (!email || email.length === 0) {
	  return Promise.reject('Email is required');
	} else {
	  try {
		const user = await User.findOne({ email });
		if (!user) {
		  return Promise.reject('User not found');
		}

		if (password && bcrypt.compareSync(password, user.password)) {
			console.log("You have entered you Old password")
			let message = `You have entered you Old password`;
			return {Message: message}
			//return Promise.reject('New password cannot be the same as previous password');
		  }	
		  

		user.password = password ? bcrypt.hashSync(password, 11) : undefined;
		return user.save();
	  } 
	  	catch (error) {
		
		console.error(error);
		throw error; 
	  }
	}
  };
  

  
//password: bcrypt.hashSync(request_Body.password, 11)

