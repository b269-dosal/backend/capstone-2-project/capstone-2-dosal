const Voucher = require('../models/Voucher');


module.exports.createVoucher = async (req, res) => {
  try {
    const voucher = new Voucher(req.body);
    await voucher.save();
    res.status(201).send(voucher);
  } catch (error) {
    res.status(400).send(error);
  }
};

module.exports.getVouchers = async (req, res) => {
  try {
    const vouchers = await Voucher.find();
    res.send(vouchers);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports.getVoucher = async (req, res) => {
  try {
    const voucher = await Voucher.findById(req.params.id);
    if (!voucher) {
      return res.status(404).send();
    }
    res.send(voucher);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports.updateVoucher = async (req, res) => {
  try {
    const voucher = await Voucher.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    if (!voucher) {
      return res.status(404).send();
    }
    res.send(voucher);
  } catch (error) {
    res.status(400).send(error);
  }
};

module.exports.deleteVoucher = async (req, res) => {
  try {
    const voucher = await Voucher.findByIdAndDelete(req.params.id);
    if (!voucher) {
      return res.status(404).send();
    }
    res.send(voucher);
  } catch (error) {
    res.status(500).send(error);
  }
};




/* 

async function archiveExpiredVouchers() {
  try {
    const expiredVouchers = await Voucher.find({ expiryDate: { $lte: Date.now() } });
    if (expiredVouchers.length > 0) {
      const archivedVouchers = await Voucher.updateMany(
        { expiryDate: { $lte: Date.now() } },
        { $set: { isArchived: true } }
      );
      console.log(`${archivedVouchers.nModified} vouchers archived`);
    } else {
      console.log('No expired vouchers found');
    }
  } catch (error) {
    console.error(error);
  }
}

module.exports = archiveExpiredVouchers;
 */