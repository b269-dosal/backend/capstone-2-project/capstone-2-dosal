// Dependencies

const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const Transaction = require("../models/Transaction");

// Add order by Unauthenticated User
module.exports.addOrder = async (data) => {

  let user = await User.findById(data.userId)
    if (!user) {
      throw new Error('User not found')
    } 

  let product = await Product.findById(data.productId)
      if (!product) {
        return res.status(404).json({ error: 'Product not found' });
      }
  
  const user_Id = user._id;
  const customerEmail = user.email;
  const product_Id = product._id;
  const product_name = product.prod_name;
  const product_price = product.prod_price;
  const quantityOrder = data.quantity || 1;
  const overTotal = product_price * quantityOrder;


  const newOrder = new Order({
    userId: user_Id,
    userEmail: customerEmail,
    productId: product_Id,
    productName: product_name,
    productPrice: product_price,
    quantity: quantityOrder,
    totalAmount: overTotal
  });

    return newOrder.save().then((user, error) => {
      if (error) {
          return (`${userId} doesnt exist, enter valid userId`)
      } else {
          return newOrder
      }
    })
}

// View All Orders - Admin only Function
module.exports.viewAllOrders = async (data) => {

  if (data.isAdmin) 
  {
    try {
      return Order.find({}).then(result => {
      const numOrders = result.length;
          const message = `Number of Total Made Orders: ${numOrders}`; 
          return {message, Orders: result}
     })   
    }
      catch (error) 	{
        throw new Error('Error');
     }		
} 
      let message = Promise.resolve('Admin Privilege Only')
			return message.then((value) => {
				return {value}
			})
}

////////////////////////////////// my first version
// Adding all Total Amount with Specifc User and Change Status to Completed
/* module.exports.checkOut_Status_Complete = async (data) => {
  try 
  {
    const userId = data.userId;
    const orders = await Order.find({ userId: userId, status: "Add to Cart" });
    let overallAmount = 0;
      
        if (orders.length === 0 || orders.length === "undefined" || orders.length === null) {
        let message = `Order cant found in our database`
        return { MESSAGE: message };
      }

      for (let i = 0; i < orders.length; i++) 
        {
                overallAmount += orders[i].totalAmount;
                orders[i].status = "Completed";
                await orders[i].save();
        }
        const User_Id = data.userId;
        return { OverAllAmount: overallAmount, USERID: User_Id, Orders: orders};
    } 
           
        catch (error) {
          console.error(error);
        }
}; */
/////////////////////////////////////////////


// [ADMIN] View Cart of certain Id
module.exports.viewIncompleteOrders = async (data) => {
  if (data.isAdmin) 
  {
    try {
      const userId = data.userId;
      const orders = await Order.find({ userId, status: "Add to Cart" });
      let sub_total = 0;

      for (let i = 0; i < orders.length; i++) 
      {
        sub_total += orders[i].totalAmount;
      }


      let message = (`Number of Order in the Cart: ${orders.length}`)
      return {MESSAGE: message, USERID: userId, SUBTOTAL: sub_total, ORDERS: orders};
    } 
      catch (error) {
      console.error(error);
    }
  } 
      let message = Promise.resolve('Admin Privilege Only')
      return message.then((value) => {
        return {value}
      })
};


/// [NON-ADMIN] View Cart of certain Id
module.exports.viewIncompleteOrders2 = async (data) => {
  if (!data.isAdmin) 
  {
    try {
      const userId = data.userId;
      const orders = await Order.find({ userId, status: "Add to Cart" });
      let sub_total = 0;

      for (let i = 0; i < orders.length; i++) 
      {
        sub_total += orders[i].totalAmount;
      }


   /*   let message = (`Number of Order in the Cart: ${orders.length}`)
      return {MESSAGE: message, USERID: userId, SUBTOTAL: sub_total, ORDERS: orders};*/
      return orders
    } 
      catch (error) {
      console.error(error);
    }
  } 
      let message = Promise.resolve('Non Admin View Only')
      return message.then((value) => {
        return {value}
      })
};

///  To Delete Order Function 
 module.exports.deleteOrder = async (data) => {
  if (!data.isAdmin) 
  {
      try 
      {
          const userId = data.userId;
          const orderId = data.orderId;

          // Check if the order with given orderId exists for the given userId
         // const order = await Order.findOne({ _id: orderId, userId, status: "Add to Cart" });
          //if (!order) {
            /*const message = "Order not found in the cart";
            return { MESSAGE: message };*/
         //   return false
       //   }

          // Remove the order
          await Order.findByIdAndRemove(orderId);

          // Recalculate the subtotal
          const orders = await Order.find({ userId, status: "Add to Cart" });
          let sub_total = 0;
          for (let i = 0; i < orders.length; i++) {
            sub_total += orders[i].totalAmount;
          }

        /*  const message = `Order with id ${orderId} has been removed from the cart`;
          return { MESSAGE: message, USERID: userId, SUBTOTAL: sub_total, ORDERS: orders };*/

          return orders
          } 
            catch (error) {
              console.error(error);
            }
  }
        let message = Promise.resolve('Non Admin Only')
      return message.then((value) => {
        return {value}
    })

};


// To Change Quantity of Order
module.exports.updateOrderQuantity = async (data) => {
  if (!data.isAdmin) 
  {
      try 
      {
        const userId = data.userId;
        const orderId = data.orderId;
        const quantity = data.quantity;

        // Check if the order with given orderId and quantity exists for the given userId
        const order = await Order.findOne({ _id: orderId, userId,  status: "Add to Cart" });
        if (!order) {
          const message = "Order not found in the cart";
          return { MESSAGE: message };
        }

        // Update the order quantity
        const productPrice = order.productPrice;
        order.quantity = quantity;
        order.totalAmount = productPrice * quantity;
        await Order.findByIdAndUpdate(orderId, order);

        // Recalculate the subtotal
        const orders = await Order.find({ userId, status: "Add to Cart" });
        let sub_total = 0;
        for (let i = 0; i < orders.length; i++) {
          sub_total += orders[i].totalAmount;
        }

    /*    const message = `Order with id ${orderId} has been updated with new quantity ${quantity}`;
        return { MESSAGE: message, USERID: userId, SUBTOTAL: sub_total, ORDERS: orders };*/

        return orders;
        
      } 
          catch (error) {
          console.error(error);
          }
  }
          let message = Promise.resolve('Non Admin only Privilege Only')
          return message.then((value) => {
            return {value}
          })
};


// Modified Version CheckOut Update Stock Complete Status, Save to Transaction Documents
module.exports.checkOutAndUpdateStock = async (data, products) => {
if (!data.isAdmin) 
 {
      try 
      {
        const userId = data.userId;
        const orders = await Order.find({ userId: userId, status: "Add to Cart" });
       
        const userEmailValues = orders.map(order => order.userEmail);
        console.log("Email Address Value: "); 
        console.log(userEmailValues); 

        // Get the first email value from the array
        const firstEmail = userEmailValues[0];
        console.log("First Email: ", firstEmail);

        let overallAmount = 0;

          

        if (orders.length === 0 || !orders) {
          let message = `Order/s not found in your Cart - database`;
          return { MESSAGE: message };
        }

        const productsPurchased = [];

        for (let i = 0; i < orders.length; i++) 
        {
              const productId = orders[i].productId;
              const quantity = orders[i].quantity;

              const product = await Product.findById(productId);

              if (!product || product.stocks - quantity < 0) 
                {
                    const message = `Product with id ${productId} is unavailable`;
                    if (product && product.stocks === 0) {
                      product.isActive = false;
                      await Product.findByIdAndUpdate(productId, product);
                    }
                    return { MESSAGE: message };
                }

              product.stocks -= quantity;
              
              if (product.stocks === 0) {
                product.isActive = false;
              }

              await Product.findByIdAndUpdate(productId, product);

              overallAmount += quantity * orders[i].productPrice;

              orders[i].status = "Completed";
              await orders[i].save();

              productsPurchased.push({
                productId: productId,
                productName: orders[i].productName,
                quantity: quantity,
                price: orders[i].productPrice
              });

                             
        }
              // Saving Data to my Transaction Schema serve as receipt
              const User_Id = data.userId;
              const transaction = new Transaction({
                userId: User_Id,
                email: firstEmail,
                amount: overallAmount,
                createdAt: new Date(),
                status: "Purchased",
                productsPurchased: productsPurchased
              });
              await transaction.save();
           

        return { OverAllAmount: overallAmount, USERID: User_Id, Orders: orders /* , INSUFFICIENTorOUTOFSTOCK: unavailable */  };
      } 
          catch (error) {
            console.error(error);
          }
  }

            let message = Promise.resolve('Non Admin Only')
            return message.then((value) => {
              return {value}
          })
}


///////////
module.exports.transactionUser = async (data) => {

/* if (!data.isAdmin) 
    {*/
      try 
      {
        return Transaction.find({userId : data.userId}).then(result => {
         if (result.length > 0){
          console.log(result)
          return result
          }
            else 
            {
              return false
            }

          })  
  
     }
     
            catch (error) {
            console.log(error)
            }


   
/*}
        let message = Promise.resolve('Non Admin Only')
        return message.then((value) => {
          return {value}
        })*/
}

///////

/*module.exports.transaction_schema.statics.selectTransactionById = async function (transactionId) {
  if (!data.isAdmin) 
  {
    try {
      const transaction = await this.findById(transactionId);
      if (!transaction) {
        throw new Error("Transaction not found");
      }
      // Add any additional processing or formatting logic as needed
      return transaction;

    } catch (error) {
      throw new Error(error.message);
    }
 }
    let message = Promise.resolve('Non Admin Only')
        return message.then((value) => {
          return {value}
        })
};
*/

/// Checking All Transaction Made
module.exports.transactionViewer = () => {
	return Transaction.find({})
  .sort({createdAt: -1})
  .populate('userId', 'first_Name last_Name email')
  .then(result => {
         /* const message = `ALL TRANSACTION MADE: ${result.length} `
		    	return {MESSAGE: message, TRANSACTIONS: result}*/
          
          return result
	})
}
///////////////////

module.exports.transactionViewer2 = () => {
  return Transaction.find({})
  .sort({createdAt: -1})
  .then(result => {
         /* const message = `ALL TRANSACTION MADE: ${result.length} `
          return {MESSAGE: message, TRANSACTIONS: result}*/
          
          return result
  })
}