// Dependencies
const Product = require("../models/Product");


// View All Product
module.exports.AllProduct = () => {
	return Product.find({}).then(result => {
		/*const numProducts = result.length;
        const message = `Number of Existing Product: ${numProducts}`; */
      	return result
	})
}

// Active products
module.exports.activeProduct = () => {
	return Product.find({isActive: true}).then(result => {
	  /*const numProducts = result.length;
	  const message = `Number of Active Products: ${numProducts}`;*/
	  return result
	});
  }

// Add product 
module.exports.addProduct = (data) => {
if (data.isAdmin) {
	let newProduct = new Product({
        prod_name : data.product.prod_name,
		prod_desc : data.product.prod_desc,
		prod_price : data.product.prod_price,
		stocks : data.product.stocks,
		category : data.product.category

	});
		return newProduct.save().then((product) => {
		if (product) {
			return product;
		} else {
			//let message = `Added New Products`
			return false;
			//return {Message: message, product}
		};
			});
}
		let message = Promise.resolve('User must be ADMIN to access this!')
		return message.then((value) => {
			return false
		})
}

// Find Product By ID
module.exports.getProduct = (reqParams) => {
if (reqParams.productId) {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}
	else {
		let negativeResult = `ID ${reqParams.productId} cant find in the database`
		return negativeResult;
	}
			
}

// Update Product
module.exports.updateProduct = async (data, request_params) => {
	if (data.isAdmin) 
	{
		let updateProduct = {
			prod_name : data.body.prod_name,
			prod_desc : data.body.prod_desc,
			prod_price : data.body.prod_price,
			stocks : data.body.stocks,
			//isActive: data.body.stocks > 0 ? true : false
			
			}
					try {
						// Before
					/*	const BeforeUpdate = await Product.findById(request_params.productId).then(before => {
							return before;
						})*/
						// After
						const UpdatedProduct = await Product.findByIdAndUpdate(request_params.productId, updateProduct, {new: true }).then((after, error) => {	
							if (error) {
											return false;
							} else { 
								return after
							}
						});	

					//	return { BeforeUpdate, UpdatedProduct };
							return  UpdatedProduct 
			
						}
						catch (error) 	{
											throw new Error('Error updating product details');
					 					}					
	}
				let message = Promise.resolve('Admin Privilege Only')
				return message.then((value) => {
					return {value}
				}) 
}

// Archive Product
module.exports.archiveProduct = (data, request_params) => {
	if (data.isAdmin) {
		const updateToFalseUpdate = { 	isActive: false }
		
		return Product.findByIdAndUpdate(request_params.productID, updateToFalseUpdate).then((update, error) => {
			if (error) {
				return false;
			} else {
			//	return (`Product Id "${request_params.productID}" has been Archive`);
			return Product.findById(request_params.productID).then(result => {
				return result;
			})		
			};
		});
		};
			let message = Promise.resolve('Admin Privilege Only')
			return message.then((value) => {
				return {value}
			}) 
}


module.exports.unarchiveProduct = (data, request_params) => {
	if (data.isAdmin) {
		const updateToTrueUpdate = { 	isActive: true }
		
		return Product.findByIdAndUpdate(request_params.productID, updateToTrueUpdate).then((update, error) => {
			if (error) {
				return false;
			} else {
			//	return (`Product Id "${request_params.productID}" has been Archive`);
			return Product.findById(request_params.productID).then(result => {
				return result;
			})		
			};
		});
		};
			let message = Promise.resolve('Admin Privilege Only')
			return message.then((value) => {
				return {value}
			}) 
}

// Search Product By Name
 module.exports.ProductSearch_KeyWord = async (data) =>{
	if (data.isAdmin) 
	{
		try {

			let nameLocator = {
				prod_name : data.body.prod_name
			}
				const Search_Product_By_Name = await Product.find(nameLocator).then(result => {
					return result;

				});

			return {Search_Product_By_Name};
			}
			catch (error) 	{
				throw new Error('Error');
			 }	
	}
			let message = Promise.resolve('Admin Privilege Only')
			return message.then((value) => {
				return {value}
			})
} 
 
///
module.exports.deleteProduct = (reqParams) => {
  if (reqParams.productID) {
    return Product.findByIdAndDelete(reqParams.productID).then(result => {
      //const message = result ? 'Product deleted successfully' : 'Product not found';
      return result;
    });
  } else {
    return Promise.reject('Product ID is required for deletion');
  }
};

