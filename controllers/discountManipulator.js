const Discount = require('../models/Discount');

// Create a new discount
exports.createDiscount = async (req, res) => {
  try {
    const discount = new Discount(req.body);
    await discount.save();
    res.status(201).send(discount);
  } catch (error) {
    res.status(400).send(error);
  }
};

// Get all discounts
exports.getDiscounts = async (req, res) => {
  try {
    const discounts = await Discount.find();
    res.send(discounts);
  } catch (error) {
    res.status(500).send(error);
  }
};

// Get a discount by id
exports.getDiscountById = async (req, res) => {
  try {
    const discount = await Discount.findById(req.params.id);
    if (!discount) {
      return res.status(404).send();
    }
    res.send(discount);
  } catch (error) {
    res.status(500).send(error);
  }
};

// Update a discount by id
exports.updateDiscount = async (req, res) => {
  try {
    const discount = await Discount.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    if (!discount) {
      return res.status(404).send();
    }
    res.send(discount);
  } catch (error) {
    res.status(400).send(error);
  }
};

// Delete a discount by id
exports.deleteDiscount = async (req, res) => {
  try {
    const discount = await Discount.findByIdAndDelete(req.params.id);
    if (!discount) {
      return res.status(404).send();
    }
    res.send(discount);
  } catch (error) {
    res.status(500).send(error);
  }
};
