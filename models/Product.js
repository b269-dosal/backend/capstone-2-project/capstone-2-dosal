// Dependencies
const mongoose = require("mongoose");

// Product Schema
const product_Schema = new mongoose.Schema({
    prod_name: {
      type: String,
      required: true
    },
    prod_desc: {
      type: String,
      required: true
    },
    prod_price: {
      type: Number,
      required: true
    },
    isActive: {
      type: Boolean,
      default: true
    },
    stocks: {
      type: Number,
      required: true,
      default: 1
    },
    category: {
      type: String,
      required: true,
    }
  
  });

  module.exports = mongoose.model("Product", product_Schema);