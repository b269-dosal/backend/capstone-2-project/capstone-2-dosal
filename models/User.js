// Dependencies
const mongoose = require("mongoose");

// User Schema
const user_Schema = new mongoose.Schema({
  first_Name: {
    type: String,
    required: true
  },  
  last_Name: {
    type: String,
    required: true
  },  
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  mobileNo : {
  type : String, 
  required : [true, "Mobile No is required"]
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  purchased : [
    {
      productId : {
        type : String,
        required : [true, "Product Id is required"]
      },
      purchasedOn : {
        type : Date,
        default : new Date()
      },
      status : {
        type : String,
        default : "Purchased"
      }
    }
  ]
  
});


module.exports = mongoose.model("User", user_Schema);