// Dependencies
const mongoose = require("mongoose");

// Product Schema
const transaction_schema = new mongoose.Schema({
    userId: {
      type: String,
      ref: 'User',
      required: true
    },
    email: {
    type: String,
    required: true
    },
    amount: {
      type: Number,
      required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    status: {
      type: String,
      default: "Processing"
    },
    productsPurchased: [
        {
          productId: {
            type: String,
            required: true
          },
          productName: {
            type: String,
            required: true
          },
          quantity: {
            type: Number,
            required: true
          },
          price: {
            type: Number,
            required: true
          }
        }
      ]
  
  });

module.exports = mongoose.model("Transaction", transaction_schema);

