// Dependencies
const mongoose = require("mongoose");

const order_Schema = new mongoose.Schema({
  
  userId: {
    type: String,
    required: true
  }, 
  userEmail: {
    type: String,
    required: true
  },
  productId: {
    type: String,
    required: true
  },
  productName: {
    type: String,
    required: true
  },
  productPrice: {
    type: Number,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
    default: 1
   },
  totalAmount: {
    type: Number,
    required: true
  }, 
  purchasedOn: {
    type: Date,
    default: Date.now
  },
  status: {
    type: String,
    required: true,
    default: "Add to Cart"
  }

});

module.exports = mongoose.model("Order", order_Schema);
